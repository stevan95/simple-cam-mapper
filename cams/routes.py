from flask import render_template, request, redirect
from cams import app, db
from cams.models import Camera
import re

@app.route('/', methods=['POST', 'GET'])
@app.route('/dodaj', methods=['POST', 'GET'])
def add_cam():
    if request.method == 'GET':
        return render_template('add_cam.html')
    elif request.method == 'POST':
        lon = request.form['lon']
        lat = request.form['lat']
        if re.match(r'^-?\d+(?:\.\d+)?$', lon) is None or re.match(r'^-?\d+(?:\.\d+)?$', lat) is None:
            return 'Greska pri unosu duzine i sirine'
        cam = Camera(lon=lon, lat=lat)
        try:
            db.session.add(cam)
            db.session.commit()
            return 'Kamera je dodata'
        except:
            return 'Greska, pokusajte opet'
    else:
        return '123'